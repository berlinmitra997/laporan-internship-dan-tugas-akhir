IEEE Explore
1 - https://ieeexplore.ieee.org/document/7814895 - Development Decision Support System of Choosing Medicine using TOPSIS Method
2 - https://ieeexplore.ieee.org/document/7947922 - Vendor Evaluation and Ranking system Using TOPSIS for Air Conditioners Manufacturer 
3 - https://ieeexplore.ieee.org/document/8009725 - Multi-Criteria Decision-Making with Imprecise Scores and BF-TOPSIS
4 - https://ieeexplore.ieee.org/document/8407795 - TOPSIS based Earth Station Marking Method 
5 - https://ieeexplore.ieee.org/document/7532165 - Quality Evaluation on Diesel Engine with Improved TOPSIS Based on Information Entropy 
6 - https://ieeexplore.ieee.org/document/7515909 - Ranking Web Service for High Quality by Applying Improved Entropy-TOPSIS Method
7 - https://ieeexplore.ieee.org/document/7528164 - Evaluation of Efficiency of Torrential Protective Structures With New BF-TOPSIS Methods
8 - https://ieeexplore.ieee.org/document/8027559 - Identify influential nodes in complex networks based on Modified TOPSIS
9 - https://ieeexplore.ieee.org/document/8276349 - AHP-TOPSIS on Selection of New University Students and the Prediction of Future Employment
10 - https://ieeexplore.ieee.org/document/7806327 - Product aspect ranking using sentiment analysis and TOPSIS
11 - https://ieeexplore.ieee.org/document/7764100 - Determination of optimal component maintenance process for RCAM of power transmission system using TOPSIS method
12 - https://ieeexplore.ieee.org/document/8463131 - Study on the Quality Evaluation Model of Diesel Engine with ANP and TOPSIS Method 
13 - https://ieeexplore.ieee.org/document/7805055 - CCSD and TOPSIS Methodology for selecting supplier in a Paper Company
14 - https://ieeexplore.ieee.org/document/6997012 - SUPERMARKET FOOD SAFETY EVALUATION BASED ON TOPSIS METHOD 
15 - https://ieeexplore.ieee.org/document/7798173 - Evaluating Human Resource Competitiveness Based on an Improved TOPSIS Method: The Case of Automotive Industry
16 - https://ieeexplore.ieee.org/document/8089277 - Implementation of TOPSIS method in the selection process of scholarship grantee (case study: BAZIS South Jakarta)
17 - https://ieeexplore.ieee.org/document/7962881 - Estimating supplier�s hidden quality costs with Taguchi quality loss function and Topsis method 
18 - https://ieeexplore.ieee.org/document/8243468 - Research on Retrofit and Evaluation of Smart Grid Equipment Based on Improved TOPSIS Method
19 - https://ieeexplore.ieee.org/document/7369640 - Risk Evaluation of Value Assessment in IPR Pledge Financing Based on Interval Value TOPSIS Method 
20 - https://www.sciencedirect.com/science/article/pii/S0957417417305225 - The behavioral TOPSIS